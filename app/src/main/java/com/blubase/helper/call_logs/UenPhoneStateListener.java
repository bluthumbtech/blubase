package com.blubase.helper.call_logs;

import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;

/**
 * Created by Ketan on 6/10/16.
 */

public class UenPhoneStateListener extends PhoneStateListener {

    int rssi;
    private static final String LOG_TAG = "UenPhoneStateListener";
    PhoneStateCallback phoneStateCallback;

    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);
        rssi = signalStrength.getGsmSignalStrength();
//        BLog.e(LOG_TAG, ""+rssi);
        phoneStateCallback.onCellularSignalChange(rssi);
    }
}
