package com.blubase.model;

/**
 * Created by Ketan on 3/10/16.
 */

public class HomeModel {

    String title;
    int icon;
    String description;
    String target;
    int color;
    String iconString;

    public HomeModel(String title, int icon, String description, String target, int color, String iconString) {
        this.title = title;
        this.icon = icon;
        this.description = description;
        this.target = target;
        this.color = color;
        this.iconString = iconString;
    }

    public String getIconString() {
        return iconString;
    }

    public void setIconString(String iconString) {
        this.iconString = iconString;
    }

    public int getIcon() {
        return icon;
    }

    public String getTarget() {
        return target;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
