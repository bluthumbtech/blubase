package com.blubase.viewholder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.blubase.R;

/**
 * Created by ketan on 10/24/16.
 */

public class HeaderViewHolder extends RecyclerView.ViewHolder{

    public TextView titleTxt;
    Activity activity;

    public HeaderViewHolder(View itemView, Activity activity) {
        super(itemView);
        this.activity = activity;
        titleTxt = (TextView)itemView.findViewById(R.id.header_txt);
    }

    public TextView getTitleTxt() {
        return titleTxt;
    }

    public void setTitleTxt(TextView titleTxt) {
        this.titleTxt = titleTxt;
    }
}
