package com.blubase.activity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.blubase.R;
import com.blubase.adapter.IntroScreenViewPagerAdapter;
import com.blubase.views.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

public class IntroActivity extends BaseActivity {
    private static final String LOG_TAG = "IntroActivity";

    public ViewPager getIntroScreenViewPager() {
        return introScreenViewPager;
    }

    private ViewPager introScreenViewPager;
    private CirclePageIndicator introCirPageIndicator;
    private IntroScreenViewPagerAdapter screenViewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_intro);

        introScreenViewPager = (ViewPager) findViewById(R.id.intro_screen_view_pager);
        introCirPageIndicator = (CirclePageIndicator) findViewById(R.id.intro_screen_circular_page_indicator);

        screenViewPagerAdapter = new IntroScreenViewPagerAdapter(getSupportFragmentManager(),getIntroScreens());
        introScreenViewPager.setAdapter(screenViewPagerAdapter);
        introScreenViewPager.setOffscreenPageLimit(getIntroScreens().size());
        introCirPageIndicator.setViewPager(introScreenViewPager);
    }

    private List<IntroScreen> getIntroScreens(){
        List<IntroScreen> introScreens = new ArrayList<>();
        introScreens.add(new IntroScreen("Create and Share live images", "express your witty mood and smile", R.drawable.illustration1, R.color.intro_screen_color_1));
        introScreens.add(new IntroScreen("Follow Crazy people and there favourite story", "meet new people and explore products that you will love", R.drawable.illustration2, R.color.intro_screen_color_2));
        introScreens.add(new IntroScreen("Start a store here, add product and Be relax", "Nautanki baba will make you rich.", R.drawable.illustration3, R.color.intro_screen_color_3));
        introScreens.add(new IntroScreen("Hit the post - kick the post kill time - Earn points ", "Hahahahahahah LOL, ROFL", R.drawable.illustration4, R.color.intro_screen_color_4));
        return introScreens;
    }

    public class IntroScreen {
        public String heading;
        public String title;
        public int introImage;
        public int introColor;

        public IntroScreen(String heading, String title, int introImage, int introColor){
            this.heading = heading;
            this.title = title;
            this.introImage = introImage;
            this.introColor = introColor;
        }
    }
}
