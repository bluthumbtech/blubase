package com.blubase.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ketan on 15/10/16.
 */

public class GenericResponse {

    @SerializedName("success")
    private String success;

//    @SerializedName("error")
//    private String error;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

//    public String getError() {
//        return error;
//    }
//
//    public void setError(String error) {
//        this.error = error;
//    }
}
