package com.blubase.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;
import com.blubase.R;

import bluthumb.common.logs.BLog;

/**
 * Created by Ketan on 4/10/16.
 */

public class SignupFragment extends Fragment {

    private View rootView;
    TextView backTxt;
    FragmentTransaction ft;
    private final static String LOG_TAG = "SignupFragment";
    Button signupBtn;

    private Tracker mTracker;
    final String screenName = "Signup Screen";

    @Override
    public void onResume() {
        super.onResume();
        BLog.i(LOG_TAG, "Screen name: " + screenName);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.signup_layout, container, false);
        initViews();
        return rootView;
    }

    private void initViews(){
        backTxt = (TextView)rootView.findViewById(R.id.back);
        signupBtn = (Button)rootView.findViewById(R.id.signup_tt);
        backTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BLog.e(LOG_TAG, "click");
                getFragmentManager().popBackStack();
            }
        });
        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(new OTPFragment());
            }
        });
    }

    private void addFragment(Fragment fragment){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.replace(R.id.onboard_frame, fragment);
        ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }

}
