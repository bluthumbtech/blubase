package com.blubase.model.response;

import com.google.gson.annotations.SerializedName;
import com.blubase.model.UserModel;

import java.util.ArrayList;

/**
 * Created by Ketan on 6/10/16.
 */

public class LoginResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("msg")
    private ArrayList<UserModel> msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<UserModel> getMsg() {
        return msg;
    }

    public void setMsg(ArrayList<UserModel> msg) {
        this.msg = msg;
    }
}
