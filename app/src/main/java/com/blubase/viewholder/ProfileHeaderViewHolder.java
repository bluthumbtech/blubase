package com.blubase.viewholder;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blubase.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by aman on 6/12/16.
 */

public class ProfileHeaderViewHolder extends RecyclerView.ViewHolder {
    private static final String LOG_TAG = "ProfileHeaderViewHolder";

    private Context context;
    private CircleImageView profileImage;
    private LinearLayout profileSearchLl,profileInfo;
    private ImageView profileShare, profileMenu;
    private TextView profileName, profileEmail, profileDetails, profile_info_post_txt, profile_info_hits_txt, profile_info_follow_txt, profile_info_follower_txt;
    private View profile_dotted_line_view, profile_dotted_line_view1;

    public ProfileHeaderViewHolder(Context context, ViewGroup parent, LayoutInflater inflater) {
        super(getViewHolderView(inflater, parent));
        this.context = context;
        initView(itemView);
        profile_dotted_line_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        profile_dotted_line_view1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    private void initView(View itemView) {
        profileImage = (CircleImageView) itemView.findViewById(R.id.profile_iv);
        profileSearchLl = (LinearLayout) itemView.findViewById(R.id.profile_main_search_ll);
        profileInfo = (LinearLayout) itemView.findViewById(R.id.profile_info_ll);
        profileShare = (ImageView) itemView.findViewById(R.id.profile__share_iv);
        profileMenu = (ImageView) itemView.findViewById(R.id.profile_over_flow_iv);
        profileName = (TextView) itemView.findViewById(R.id.profile_name_txt);
        profileEmail = (TextView) itemView.findViewById(R.id.profile_email_txt);
        profileDetails = (TextView) itemView.findViewById(R.id.profile_detail_txt);
        profile_info_post_txt = (TextView) itemView.findViewById(R.id.profile_info_post_txt);
        profile_info_hits_txt = (TextView) itemView.findViewById(R.id.profile_info_hits_txt);
        profile_info_follow_txt = (TextView) itemView.findViewById(R.id.profile_info_follow_txt);
        profile_info_follower_txt = (TextView) itemView.findViewById(R.id.profile_info_follower_txt);
        profile_dotted_line_view =  itemView.findViewById(R.id.profile_dotted_line_view);
        profile_dotted_line_view1 =  itemView.findViewById(R.id.profile_dotted_line_view1);
    }

    private static View getViewHolderView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.profile_header_view_holder, parent, false);
    }


    public void setData(final JSONObject profile){
        try {
            if (profile != null) {
                if (profile.getString("pic") != null && profile.getString("pic").length() > 0) {
                    Picasso.with(context).load(profile.getString("pic")).into(profileImage);
                }else {
                    Picasso.with(context).load(R.drawable.profile_pic).into(profileImage);
                }

                profileName.setText(profile.getString("nm"));
                profileEmail.setText(profile.getString("uid"));
                profileDetails.setText(profile.getString("ab"));

                profile_info_post_txt.setText(profile.getJSONObject("info").getString("post"));
                profile_info_hits_txt.setText(profile.getJSONObject("info").getString("hits"));
                profile_info_follow_txt.setText(profile.getJSONObject("info").getString("fl"));
                profile_info_follower_txt.setText(profile.getJSONObject("info").getString("flr"));

                profileMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showPopupList(profile, profileMenu);
                    }
                });
            }
        }catch (Exception e){}
    }

    private void showPopupList(JSONObject jsonObject, View anchorView) {
        try {
            PopupMenu popupMenu = new PopupMenu(context, anchorView);
            JSONArray menu = jsonObject.getJSONArray("menu");
            for (int i=0; i < menu.length(); i++){
                popupMenu.getMenu().add(menu.getJSONObject(i).getString("mt"));
            }
            popupMenu.show();
        }catch (Exception e){}
    }
}
