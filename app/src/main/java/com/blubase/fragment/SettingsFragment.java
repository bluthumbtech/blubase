package com.blubase.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;
import com.blubase.R;
import com.blubase.base.AppConstants;
import com.blubase.helper.AppUtil;
import com.blubase.helper.UenPreferences;

import bluthumb.common.logs.BLog;

/**
 * Created by Ketan on 4/10/16.
 */

public class SettingsFragment extends Fragment {

    private View rootView;
    TextView backTxt, appVersionTxt, aboutTxt, logoutTxt;
    FragmentTransaction ft;
    private final static String LOG_TAG = "SettingsFragment";
    SwitchCompat vibrateSwitch;
    UenPreferences preferences;

    private Tracker mTracker;
    final String screenName = "Settings Screen";

    @Override
    public void onResume() {
        super.onResume();
        BLog.i(LOG_TAG, "Screen name: " + screenName);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.settings_layout, container, false);
        preferences = new UenPreferences(getActivity());
        initViews();
        return rootView;
    }

    private void initViews(){
        backTxt = (TextView)rootView.findViewById(R.id.back);
        appVersionTxt = (TextView)rootView.findViewById(R.id.app_version);
        aboutTxt = (TextView)rootView.findViewById(R.id.about);
        logoutTxt = (TextView)rootView.findViewById(R.id.logout);
        vibrateSwitch = (SwitchCompat)rootView.findViewById(R.id.vibrate_switch);
        if(preferences.isVibrate()){
            vibrateSwitch.setChecked(true);
        }else{
            vibrateSwitch.setChecked(false);
        }
        backTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
        vibrateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                preferences.setIsVibrate(isChecked);
            }
        });
        appVersionTxt.setText("App Version: "+ AppConstants.APP_VERSION_NAME);
        logoutTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.clearPreferences();
                AppUtil.goToLogin(getActivity());
            }
        });
    }

}
