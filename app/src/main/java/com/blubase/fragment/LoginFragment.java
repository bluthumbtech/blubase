package com.blubase.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.analytics.Tracker;
import com.blubase.R;
import com.blubase.base.AppConstants;
import com.blubase.helper.UenPreferences;
import com.blubase.helper.ViewUtil;
import com.blubase.helper.callbacks.FragmentCallback;

import bluthumb.common.logs.BLog;

/**
 * Created by Ketan on 4/10/16.
 */

public class LoginFragment extends Fragment {
    private static final String LOG_TAG = "LoginFragment";

    private View rootView;
    private ImageView backBt;
    private Button signUpBtn;
    private EditText usernameEdt, mobileNumEdt, passwordEdt, referCodeEdt;
    private TextView forgotPasswordTxt, fbSignUpTxt, googleSignUpText;
    private RelativeLayout nameRl, referCodeRl;
    private Activity activity;
    UenPreferences preferences;
    private Tracker mTracker;
    final String screenName = "Login Screen";
    RelativeLayout parentView;
    MaterialDialog.Builder materialDialog;
    MaterialDialog dialog;

    private String page_code = null;
    private FragmentCallback fragmentCallback;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCallback) {
            fragmentCallback = (FragmentCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentCallbackListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            page_code = bundle.getString(AppConstants.FRAGMENT_PAGE_CODE_KEY);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BLog.i(LOG_TAG, "Screen name: " + screenName);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.login_layout, container, false);
        activity = getActivity();
        initViews();
        ViewUtil.hideKeyboard(activity);

        if (page_code != null && page_code.equals(AppConstants.SIGN_IN_PAGE_TARGET)) {
            signUpBtn.setText("Sign in");
            nameRl.setVisibility(View.GONE);
            referCodeRl.setVisibility(View.GONE);
        }else {
            signUpBtn.setText("Sign up");
            nameRl.setVisibility(View.VISIBLE);
            referCodeRl.setVisibility(View.VISIBLE);
        }
        return rootView;
    }

    private void initViews(){
        parentView = (RelativeLayout)rootView.findViewById(R.id.login_head);
        backBt = (ImageView) rootView.findViewById(R.id.action_back);
        usernameEdt = (EditText)rootView.findViewById(R.id.username_edt);
        mobileNumEdt = (EditText)rootView.findViewById(R.id.mobile_edt);
        passwordEdt = (EditText)rootView.findViewById(R.id.password_edt);
        referCodeEdt = (EditText)rootView.findViewById(R.id.refer_edt);
        forgotPasswordTxt = (TextView)rootView.findViewById(R.id.forgot_password_lg);
        signUpBtn = (Button)rootView.findViewById(R.id.sign_up_bt);
        fbSignUpTxt = (TextView)rootView.findViewById(R.id.fb_sign_up_bt);
        googleSignUpText = (TextView)rootView.findViewById(R.id.google_sign_up_bt);
        nameRl = (RelativeLayout) rootView.findViewById(R.id.name_rl);
        referCodeRl = (RelativeLayout) rootView.findViewById(R.id.refer_code_rl);

        backBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (page_code != null && page_code.equals(AppConstants.SIGN_IN_PAGE_TARGET)){
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.FRAGMENT_PAGE_CODE_KEY, AppConstants.HOME_PAGE_TARGET);
                    fragmentCallback.onInFragmentButtonClick(bundle);
                }else if (page_code != null && page_code.equals(AppConstants.JOIN_US_PAGE_TARGET)){
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.FRAGMENT_PAGE_CODE_KEY, AppConstants.OTP_PAGE_TARGET);
                    fragmentCallback.onInFragmentButtonClick(bundle);
                }
            }
        });

        fbSignUpTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        googleSignUpText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });


        forgotPasswordTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    private void addFragment(Fragment fragment){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.replace(R.id.onboard_frame, fragment);
        ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }

    private boolean verifyLoginCredendials(){
        if(usernameEdt.getText().toString().isEmpty() && passwordEdt.getText().toString().isEmpty()){
            Toast.makeText(activity, "Username/Password is required" ,Toast.LENGTH_SHORT).show();
            return false;
        }else if(usernameEdt.getText().toString().isEmpty()){
            Toast.makeText(activity, "Username is required" ,Toast.LENGTH_SHORT).show();
            return false;
        }else if(passwordEdt.getText().toString().isEmpty()){
            Toast.makeText(activity, "Password is required" ,Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }

}
