package com.blubase.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blubase.R;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

/**
 * Created by aman on 8/12/16.
 */

public class OrderCardViewHolder extends RecyclerView.ViewHolder {

    private Context context;
    private LinearLayout order_detail_ll, order_delivery_status_ll;
    private ImageView order_image, order_status_image, order_delivery_status_1, order_delivery_status_2, order_delivery_status_3, order_delivery_status_4;
    private Button order_bt;
    private TextView order_name, order_status_txt, order_date_txt;
    private View order_card_dotted_line_view;

    public OrderCardViewHolder(Context context, ViewGroup parent, LayoutInflater inflater) {
        super(getViewHolderView(inflater, parent));
        initView(itemView);
        this.context = context;
        order_card_dotted_line_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    private void initView(View itemView) {
        order_detail_ll = (LinearLayout) itemView.findViewById(R.id.order_detail_ll);
        order_delivery_status_ll = (LinearLayout) itemView.findViewById(R.id.order_delivery_status_ll);
        order_name = (TextView) itemView.findViewById(R.id.order_name);
        order_status_txt = (TextView) itemView.findViewById(R.id.order_status_txt);
        order_date_txt = (TextView) itemView.findViewById(R.id.order_date_txt);
        order_image = (ImageView) itemView.findViewById(R.id.order_image);
        order_status_image = (ImageView) itemView.findViewById(R.id.order_status_image);
        order_delivery_status_1 = (ImageView) itemView.findViewById(R.id.order_delivery_status_1);
        order_delivery_status_2 = (ImageView) itemView.findViewById(R.id.order_delivery_status_2);
        order_delivery_status_3 = (ImageView) itemView.findViewById(R.id.order_delivery_status_3);
        order_delivery_status_4 = (ImageView) itemView.findViewById(R.id.order_delivery_status_4);
        order_bt = (Button) itemView.findViewById(R.id.order_bt);
        order_card_dotted_line_view = itemView.findViewById(R.id.order_card_dotted_line_view);
    }

    private static View getViewHolderView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.order_card_view_holder, parent, false);
    }

    public void setData(final JSONObject order){
        try {
            if (order != null) {
                if (order.getString("pic") != null && order.getString("pic").length() > 0) {
                    Picasso.with(context).load(order.getString("pic")).into(order_image);
                }else {
                    Picasso.with(context).load(R.drawable.profile_pic).into(order_image);
                }

                order_name.setText(order.getString("nm"));
                order_status_txt.setText(order.getString("st"));
                order_date_txt.setText(order.getString("tm"));

                int padding_10 = (int) context.getResources().getDimension(R.dimen.padding_size_10);
                int padding_20 = (int) context.getResources().getDimension(R.dimen.padding_size_20);
                if ("Approved".equals(order.getString("st"))){
                    order_card_dotted_line_view.setVisibility(View.VISIBLE);
                    order_delivery_status_ll.setVisibility(View.VISIBLE);
                    order_bt.setVisibility(View.VISIBLE);
                    order_delivery_status_1.setImageDrawable(context.getResources().getDrawable(R.drawable.tick_icon));
                    order_status_txt.setTextColor(context.getResources().getColor(R.color.app_color));
                    order_status_image.setImageDrawable(context.getResources().getDrawable(R.drawable.approved));
                    order_detail_ll.setPadding(padding_10,padding_10,padding_10,padding_10);
                }else {
                    order_card_dotted_line_view.setVisibility(View.GONE);
                    order_delivery_status_ll.setVisibility(View.GONE);
                    order_bt.setVisibility(View.GONE);
                    order_status_txt.setTextColor(context.getResources().getColor(R.color.red_light));
                    order_status_image.setImageDrawable(context.getResources().getDrawable(R.drawable.cancel));
                    order_detail_ll.setPadding(padding_10,padding_10,padding_10,padding_20);
                }
            }
        }catch (Exception e){}
    }
}

