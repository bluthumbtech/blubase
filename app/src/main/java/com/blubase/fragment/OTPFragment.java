package com.blubase.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.SmsMessage;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.blubase.R;
import com.blubase.base.AppConstants;
import com.blubase.helper.AppUtil;
import com.blubase.helper.ViewUtil;
import com.blubase.helper.callbacks.FragmentCallback;

import bluthumb.common.logs.BLog;

/**
 * Created by Ketan on 7/10/16.
 */

public class OTPFragment extends Fragment {
    private static final String LOG_TAG = "OTPFragment";

    private View rootView;
    private ImageView backBt;
    private EditText mobileEdt, otpEdt;
    private TextView backTxt, editMobileTxt, optTimerText;
    private Button otpVerifyBt;

    private FragmentCallback fragmentCallback;

    private Tracker mTracker;
    final String screenName = "OTP Screen";


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCallback) {
            fragmentCallback = (FragmentCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentCallbackListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.provider.Telephony.SMS_RECEIVED");
            getActivity().registerReceiver(smsMessageReceiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        BLog.i(LOG_TAG, "Screen name: " + screenName);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.otp_layout, container, false);
        initViews();
        return rootView;
    }

    private void initViews(){
        backBt = (ImageView) rootView.findViewById(R.id.action_back);
        mobileEdt = (EditText) rootView.findViewById(R.id.mobile_edt);
        otpEdt = (EditText) rootView.findViewById(R.id.otp_edt);
        editMobileTxt = (TextView)rootView.findViewById(R.id.mobile_edit_text);
        optTimerText = (TextView)rootView.findViewById(R.id.otp_timer_text);
        otpVerifyBt = (Button) rootView.findViewById(R.id.otp_verify_bt);


        backBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BLog.e(LOG_TAG, "click");
                getFragmentManager().popBackStack();
            }
        });

        otpVerifyBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.FRAGMENT_PAGE_CODE_KEY, AppConstants.HOME_PAGE_TARGET);
                fragmentCallback.onInFragmentButtonClick(bundle);
            }
        });


    }

    private void handleSuccesfullOtpRead(String otp_str) {
        BLog.e("OTP", "handleSuccesfullOtpRead - "+otp_str);
        Toast.makeText(getActivity(), "OTP Detected", Toast.LENGTH_SHORT).show();
        ViewUtil.hideKeyboard(getActivity());
//        verifyOtpNew(otp_str);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregister_receiver();
    }

    private void unregister_receiver() {
        try {
            if (smsMessageReceiver != null) {
                getActivity().unregisterReceiver(smsMessageReceiver);
                smsMessageReceiver = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private BroadcastReceiver smsMessageReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            try{
                Bundle bundle = intent.getExtras();
                SmsMessage[] msgs = null;
                String str = "";
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++) {
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);

                        String message_body = msgs[i].getMessageBody().toString();
                        String sender = msgs[i].getDisplayOriginatingAddress();

                        message_body = message_body.toLowerCase();

                        BLog.e(LOG_TAG, "sender - " + sender);
                        BLog.e(LOG_TAG, "OTPLISTENERE message_body - " + message_body);
                        BLog.e(LOG_TAG, "message_body Time - " + msgs[i].getTimestampMillis());
                        BLog.e(LOG_TAG, "extractNumber - " + AppUtil.extractNumber(message_body));

                        if(!sender.toLowerCase().contains(AppConstants.SMS_ID.toLowerCase())){
                            return;
                        }

                        if(message_body.matches(".*\\d+.*")){
                            BLog.e("OTP", "true con");
                            handleSuccesfullOtpRead(AppUtil.extractNumber(message_body));
                        }else{
                            BLog.e("OTP", "false con");
                        }
                    }
                }
            }catch (Exception e){
                BLog.e(LOG_TAG, ""+e);
            }
        }
    };

}
