package com.blubase.base;

import android.app.Application;

import com.blubase.R;
import com.blubase.helper.AppUtil;
import com.blubase.helper.UenPreferences;

import bluthumb.common.logs.BLog;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by ketan on 2/16/16.
 */
public class AppController extends Application {

    private static final String LOG_TAG = "AppController";
    private UenPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        BLog.setShouldPrint(true);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/roboto_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        preferences = new UenPreferences(this);
        preferences.setMultipleDevices(false);
        BLog.e(LOG_TAG, "device id - "+ AppUtil.getDeviceId(this));
    }

}
