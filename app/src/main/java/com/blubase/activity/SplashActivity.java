package com.blubase.activity;

import android.os.Bundle;

import com.blubase.helper.AppUtil;
import com.blubase.helper.UenPreferences;

public class SplashActivity extends BaseActivity {

    UenPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = new UenPreferences(this);
        if(preferences.isLoggedIn()){
            AppUtil.goToHome(this);
        }else{
            AppUtil.goToLogin(this);
        }
    }
}
