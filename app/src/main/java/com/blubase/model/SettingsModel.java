package com.blubase.model;

/**
 * Created by Ketan on 4/10/16.
 */

public class SettingsModel {

    boolean isVibrate;

    public boolean isVibrate() {
        return isVibrate;
    }

    public void setVibrate(boolean vibrate) {
        isVibrate = vibrate;
    }
}
