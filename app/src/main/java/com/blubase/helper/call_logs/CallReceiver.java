package com.blubase.helper.call_logs;

import android.content.Context;

import java.util.Date;

import bluthumb.common.logs.BLog;

/**
 * Created by Ketan on 4/10/16.
 */

public class CallReceiver extends PhonecallReceiver {

    private static final String LOG_TAG = "CallReceiver";

    @Override
    protected void onIncomingCallReceived(Context ctx, String number, Date start) {
        BLog.e(LOG_TAG, "onIncomingCallReceived - "+number);
    }

    @Override
    protected void onIncomingCallAnswered(Context ctx, String number, Date start) {
        BLog.e(LOG_TAG, "onIncomingCallAnswered - "+number);
    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end) {
        BLog.e(LOG_TAG, "onIncomingCallEnded - "+number);
    }

    @Override
    protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
        BLog.e(LOG_TAG, "onOutgoingCallStarted - "+number);
    }

    @Override
    protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end) {
        BLog.e(LOG_TAG, "onOutgoingCallEnded - "+number);
    }

    @Override
    protected void onMissedCall(Context ctx, String number, Date start) {
        BLog.e(LOG_TAG, "onMissedCall - "+number);
    }

}
