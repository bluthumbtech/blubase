package com.blubase.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blubase.R;

import bluthumb.common.logs.BLog;

/**
 * Created by Ketan on 4/10/16.
 */

public class ForgotPasswordFragment extends Fragment {

    private View rootView;
    TextView backTxt;
    FragmentTransaction ft;
    private final static String LOG_TAG = "ForgotPasswordFragment";

    final String screenName = "Forgot Password Screen";

    @Override
    public void onResume() {
        super.onResume();
        BLog.i(LOG_TAG, "Screen name: " + screenName);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.forgot_password_layout, container, false);
        initViews();
        return rootView;
    }

    private void initViews(){
        backTxt = (TextView)rootView.findViewById(R.id.back);
        backTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BLog.e(LOG_TAG, "click");
                getFragmentManager().popBackStack();
            }
        });
    }

}
