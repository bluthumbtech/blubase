package com.blubase.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blubase.R;
import com.blubase.adapter.OrderListAdapter;
import com.blubase.helper.AppUtil;

import org.json.JSONArray;
import org.json.JSONObject;

public class OrderActivity extends BaseActivity {
    private static final String LOG_TAG = "OrderActivity";
    private LinearLayout toolbar;
    private RecyclerView orderRecyclerView;
    private TextView pageTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        initViews();

        pageTitle.setText("My Order");

        String profileString  = AppUtil.loadJSONFromAsset(this, "order_test.json");
        JSONObject json = null;
        JSONArray orders = null;
        try {
            json = new JSONObject(profileString);
            orders = json.getJSONArray("ors");
        }catch (Exception e){}

        //setViewsData(profile);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        orderRecyclerView.setLayoutManager(layoutManager);
        OrderListAdapter adapter = new OrderListAdapter(this, orders);
        orderRecyclerView.setAdapter(adapter);
    }

    private void initViews() {
        toolbar = (LinearLayout) findViewById(R.id.common_toolbar);
        pageTitle = (TextView) findViewById(R.id.toolbar_title);
        orderRecyclerView = (RecyclerView) findViewById(R.id.order_list_rv);

    }

}
