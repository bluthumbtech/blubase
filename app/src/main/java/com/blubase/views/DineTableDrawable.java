package com.blubase.views;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import bluthumb.common.logs.BLog;

/**
 * Created by ketan on 10/21/16.
 */

public class DineTableDrawable extends Drawable {

    // TODO Sample variables, these don't take density into account
    private static final int TICK_COUNT = 10;
    private static final int RING_THICKNESS = 50;
    private static final int TICK_THICKNESS = 15;
    private static final int LONG_TICK = 30;
    private static final int SHORT_TICK = 10;
    private static final String LOG_TAG = "DineTableDrawable";
    private int noOfChairs = 0;

    private final Paint mRingPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint mTickPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public DineTableDrawable() {
        mRingPaint.setColor(Color.parseColor("#AF88D3"));
//        mRingPaint.setStrokeWidth(RING_THICKNESS);
//        mRingPaint.setStyle(Paint.Style.STROKE);
        mTickPaint.setColor(Color.parseColor("#726187"));
        mTickPaint.setStrokeWidth(TICK_THICKNESS);
        mTickPaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    public void draw(Canvas canvas) {
        final Rect bounds = getBounds();
        final int h = bounds.height();
        final float cx = bounds.exactCenterX();
        final float cy = bounds.exactCenterY();
        final float rotationAnglePerTick = 360f / TICK_COUNT;

        // Draw the ring (using the stroke, offset by half of the stroke width
        // since the stroke is drawn around the circle (not inside or outside)
        canvas.drawCircle(cx, cy, 177, mRingPaint);

        for (int i = 0; i < TICK_COUNT; i++) {
            // Check if it's one of the ticks on the top, left, right, or bottom
            float tickHeight = i % (TICK_COUNT / 4) == 0 ? noOfChairs : noOfChairs;

            // For each tick, rotate the canvas by the angle per tick and the tick
            // number, then draw a rectangle along the bottom. Since the canvas is
            // rotating, you can just continue to draw a simple rectangle and not
            // worry about angles. The canvas should rotate around the center point.
            //
            // Not covered here: clipping the ticks to stay within the ring bounds.
            canvas.save();

            canvas.rotate(rotationAnglePerTick * i, cx, cy);
            canvas.drawRect(
                    cx - (TICK_THICKNESS / 2),
                    h - tickHeight,
                    cx + (TICK_THICKNESS / 2),
                    h,
                    mTickPaint);
//            canvas.drawRect(
//                    50,
//                    50,
//                    80,
//                    80,
//                    mTickPaint);
            BLog.e(LOG_TAG, "rect_1: "+(cx - (TICK_THICKNESS / 2)));
            BLog.e(LOG_TAG, "rect_2: "+(h - tickHeight));
            BLog.e(LOG_TAG, "rect_3: "+(cx + (TICK_THICKNESS / 2)));
            BLog.e(LOG_TAG, "rect_4: "+(h));
            BLog.e(LOG_TAG, "cx: "+cx);
            BLog.e(LOG_TAG, "tickHeight: "+tickHeight);

            canvas.restore();
        }
    }

    @Override
    public void setAlpha(int alpha) {
        mRingPaint.setAlpha(alpha);
        mTickPaint.setAlpha(alpha);
        invalidateSelf();
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        mRingPaint.setColorFilter(cf);
        mTickPaint.setColorFilter(cf);
        invalidateSelf();
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    public void setNoOfChairs(int noOfChairs){
        this.noOfChairs = noOfChairs;
    }

}
