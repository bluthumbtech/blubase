package com.blubase.model;

/**
 * Created by Ketan on 13/10/16.
 */

public class NotificationModel {

    String number;
    String businessId;

    public NotificationModel(String number, String businessId) {
        this.number = number;
        this.businessId = businessId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }
}
