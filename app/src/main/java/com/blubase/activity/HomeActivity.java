package com.blubase.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.blubase.R;
import com.blubase.adapter.MainFeedAdapter;
import com.blubase.views.circularfloatingbutton.FloatingActionButton;
import com.blubase.views.circularfloatingbutton.FloatingActionMenu;
import com.blubase.views.circularfloatingbutton.SubActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String LOG_TAG = "HomeActivity";

    private Toolbar toolbar;
    private RelativeLayout cart_icon_rl;
    private RecyclerView homeRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initView();
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.circular_floating_bt);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View navHeader = navigationView.getHeaderView(0);
        if (navHeader != null) {
            LinearLayout nav_header = (LinearLayout) navHeader.findViewById(R.id.nav_header_parent_ll);
            nav_header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
                }
            });
        }

        String profileString  = loadJSONFromAsset(this);
        JSONObject jsonObject;
        JSONArray feed = null;
        try {
            jsonObject = new JSONObject(profileString);
            feed = jsonObject.getJSONArray("feed");
        }catch (Exception e){}


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        homeRecyclerView.setLayoutManager(layoutManager);
        MainFeedAdapter adapter = new MainFeedAdapter(this, feed);
        homeRecyclerView.setAdapter(adapter);

        createCircularFloatingMenu();
    }

    private void createCircularFloatingMenu() {
        // Set up the white button on the lower right corner
        // more or less with default parameter
//        final ImageView fabIconNew = new ImageView(this);
//        FrameLayout.LayoutParams params  = new FrameLayout.LayoutParams( (int)getResources().getDimension(R.dimen.action_button_size), (int)getResources().getDimension(R.dimen.action_button_size));
//        fabIconNew.setLayoutParams(params);
//        fabIconNew.setImageDrawable(getResources().getDrawable(R.drawable.floating_button_selector));
        final FloatingActionButton rightLowerButton = new FloatingActionButton.Builder(this)
//                .setContentView(fabIconNew)
                .build();

        SubActionButton.Builder rLSubBuilder = new SubActionButton.Builder(this);
        ImageView rlIcon1 = new ImageView(this);
        ImageView rlIcon2 = new ImageView(this);
        ImageView rlIcon3 = new ImageView(this);

        rlIcon1.setImageDrawable(getResources().getDrawable(R.drawable.post));
        rlIcon2.setImageDrawable(getResources().getDrawable(R.drawable.post_image));
        rlIcon3.setImageDrawable(getResources().getDrawable(R.drawable.post_gif));

        // Build the menu with default options: light theme, 90 degrees, 72dp radius.
        // Set 4 default SubActionButtons
        final FloatingActionMenu rightLowerMenu = new FloatingActionMenu.Builder(this)
                .addSubActionView(rLSubBuilder.setContentView(rlIcon1).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon2).build())
                .addSubActionView(rLSubBuilder.setContentView(rlIcon3).build())
                .attachTo(rightLowerButton)
                .build();
    }


    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        cart_icon_rl = (RelativeLayout) toolbar.findViewById(R.id.cart_icon_rl);
        homeRecyclerView = (RecyclerView) findViewById(R.id.home_rv_list);

        cart_icon_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, CartActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_rate_us) {
            // Handle the camera action
        } else if (id == R.id.nav_invite_friend) {

        } else if (id == R.id.nav_offers) {
            startActivity(new Intent(HomeActivity.this, OrderActivity.class));
        } else if (id == R.id.nav_about_us) {
            startActivity(new Intent(HomeActivity.this, ProductDetailActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static String loadJSONFromAsset(Context context) {
        String json;
        try {
            InputStream is = context.getAssets().open("home_test.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (Exception ex) {
            Log.e(LOG_TAG, " Exception loadJSONFromAsset - ", ex);
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
