package com.blubase.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blubase.R;
import com.blubase.activity.IntroActivity;
import com.blubase.activity.OnBoardActivity;

/**
 * Created by aman on 5/12/16.
 */

public class IntroFragment extends Fragment {
    private static final String LOG_TAG = "IntroFragment";

    private View rootView;
    private TextView headingTxt, titleTxt, detailTxt;
    private ImageView introScreenIv;
    private RelativeLayout mainRl;
    private TextView intro_screen_enter_bt;

    private String heading, title, enter_button = null;
    int intro_image, intro_color;

    private IntroActivity introActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            heading = bundle.getString("intro_heading");
            title = bundle.getString("intro_title");
            intro_image = bundle.getInt("intro_image");
            intro_color = bundle.getInt("intro_color");
            enter_button = bundle.getString("enter_button");
        }

        introActivity = (IntroActivity) getActivity();

        Log.i(LOG_TAG, "heading - "+heading+", title - "+title+", intro image"+intro_image+", intro color"+intro_color);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.intro_screen_fragment, null);

        headingTxt = (TextView) rootView.findViewById(R.id.intro_screen_heading_txt);
        titleTxt = (TextView) rootView.findViewById(R.id.intro_screen_title_txt);
        introScreenIv = (ImageView) rootView.findViewById(R.id.intro_screen_iv);
        mainRl = (RelativeLayout) rootView.findViewById(R.id.intro_screen_main_ll);
        intro_screen_enter_bt = (TextView) rootView.findViewById(R.id.intro_screen_enter_bt);

        mainRl.setBackgroundColor(getResources().getColor(intro_color));
        headingTxt.setText(heading);
        titleTxt.setText(title);
        introScreenIv.setBackgroundDrawable(getResources().getDrawable(intro_image));

        intro_screen_enter_bt.setText(enter_button);
        intro_screen_enter_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count  = introActivity.getIntroScreenViewPager().getAdapter().getCount();
                    int position = introActivity.getIntroScreenViewPager().getCurrentItem();

                    if (position < count -1) {
                        introActivity.getIntroScreenViewPager().setCurrentItem(position+1);
                    }else if (position == count-1){
                        startActivity(new Intent(getActivity(), OnBoardActivity.class));
                        getActivity().finish();
                    }
                }
            });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
