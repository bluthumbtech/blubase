package com.blubase.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.blubase.viewholder.CartOrderCardViewHolder;
import com.blubase.viewholder.SimpleTextViewHolder;

import org.json.JSONArray;
import org.json.JSONObject;

import static com.blubase.base.AppConstants.CART_ORDER_CARD;
import static com.blubase.base.AppConstants.SIMPLE_TEXT;

/**
 * Created by aman on 8/12/16.
 */

public class CartOrderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String LOG_TAG = "CartOrderListAdapter";

    private Context context;
    private LayoutInflater layoutInflater;
    private JSONArray orderList;


    public CartOrderListAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.orderList = jsonArray;

        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    public void updateFeed(JSONArray jsonArray) {
        this.orderList = jsonArray;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case CART_ORDER_CARD:
                viewHolder = new CartOrderCardViewHolder(context, parent, layoutInflater);
                break;
            case SIMPLE_TEXT:
                viewHolder = new SimpleTextViewHolder(context, parent, layoutInflater);
                break;
            default:
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case CART_ORDER_CARD:
                CartOrderCardViewHolder orderCardViewHolder = (CartOrderCardViewHolder) holder;
                try {
                    orderCardViewHolder.setData(orderList.getJSONObject(position));
                } catch (Exception e) {
                }
                break;
            case SIMPLE_TEXT:
                SimpleTextViewHolder simpleTextViewHolder = (SimpleTextViewHolder) holder;
                try {
                    simpleTextViewHolder.setData(orderList.getJSONObject(position));
                }catch (Exception e){}
                break;
            default:
        }
    }

    @Override
    public int getItemCount() {
        if (orderList != null && orderList.length() > 0) {
            return orderList.length();
        } else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (orderList != null && orderList.length() > 0 && position < orderList.length()) {
            try {
                JSONObject jsonObject = (JSONObject) orderList.get(position);
                if ("oc".equals(jsonObject.getString("tt"))) {
                    return CART_ORDER_CARD;
                }else if ("st".equals(jsonObject.getString("tt"))){
                    return SIMPLE_TEXT;
                }else {
                    return -1;
                }
            }catch (Exception e){
                return -1;
            }
        }else  {
            return -1;
        }
    }
}