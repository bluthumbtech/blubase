package com.blubase.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blubase.R;

import org.json.JSONObject;

/**
 * Created by aman on 6/12/16.
 */

public class SimpleTextViewHolder extends RecyclerView.ViewHolder {

    private TextView simple_txt;

    public SimpleTextViewHolder(Context context, ViewGroup parent, LayoutInflater inflater) {
        super(getViewHolderView(inflater, parent));
        initView(itemView);
    }

    private void initView(View itemView) {
        simple_txt = (TextView) itemView.findViewById(R.id.simple_txt);
    }

    private static View getViewHolderView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.simple_text_view_holder, parent, false);
    }

    public void setData(final JSONObject jsonObject){
        try {
            if (jsonObject != null) {
                simple_txt.setText(jsonObject.getString("t"));
            }
        }catch (Exception e){}
    }
}
