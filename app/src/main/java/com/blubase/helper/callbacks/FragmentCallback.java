package com.blubase.helper.callbacks;

import android.os.Bundle;

/**
 * Created by aman on 3/12/16.
 */
public interface FragmentCallback {

    void onInFragmentButtonClick(Bundle bundle);
}
