package com.blubase.activity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blubase.R;
import com.blubase.views.CirclePageIndicator;

public class ProductDetailActivity extends BaseActivity {
    private static final String LOG_TAG = "ProductDetailActivity";
    private LinearLayout toolbar, cart_user_info_ll;
    private RecyclerView orderRecyclerView;
    private ViewPager product_display_view_pager;
    private CirclePageIndicator product_display_page_indicator;
    private EditText cart_price_detail_coupon_apply_edt;
    private Button cart_pay_bt;
    private ImageView product_detail_more_menu_iv, product_display_share_iv, product_display_comment_iv, product_display_like_iv, product_detail_page_quantity_minus_image, product_detail_page_quantity_plus_image;
    private TextView pageTitle, product_rating_txt, product_review_txt, product_display_name, product_display_price_txt, product_detail_page_quantity_txt,
            product_detail_delivery_address_txt, cart_price_detail_total_price_txt, cart_price_detail_coupon_apply_txt,
            cart_price_detail_discount_approved_txt, cart_price_detail_total_payable_amount_txt;
    private View product_display_dotted_line_view, product_display_dotted_line_view1, product_display_dotted_line_view2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        initViews();

        pageTitle.setText("Product Name");
        product_display_dotted_line_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        product_display_dotted_line_view1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        product_display_dotted_line_view2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    private void initViews() {
        toolbar = (LinearLayout) findViewById(R.id.common_toolbar);
        pageTitle = (TextView) findViewById(R.id.toolbar_title);
        product_rating_txt = (TextView) findViewById(R.id.product_rating_txt);
        cart_price_detail_total_price_txt = (TextView) findViewById(R.id.cart_price_detail_total_price_txt);
        cart_price_detail_total_payable_amount_txt = (TextView) findViewById(R.id.cart_price_detail_total_payable_amount_txt);
        orderRecyclerView = (RecyclerView) findViewById(R.id.cart_order_recycler_view);
        product_display_dotted_line_view = findViewById(R.id.product_display_dotted_line_view);
        product_display_dotted_line_view1 = findViewById(R.id.product_display_dotted_line_view1);
        product_display_dotted_line_view2 = findViewById(R.id.product_display_dotted_line_view2);
    }
}
