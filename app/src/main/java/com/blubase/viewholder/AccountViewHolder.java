package com.blubase.viewholder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blubase.R;

/**
 * Created by Ketan on 8/10/16.
 */

public class AccountViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView nameTxt, localityTxt;
    public ImageView logo;
    public LinearLayout selectAccLL;
    Activity activity;

    public AccountViewHolder(View itemView, Activity activity) {
        super(itemView);
        itemView.setOnClickListener(this);
        this.activity = activity;
        nameTxt = (TextView)itemView.findViewById(R.id.name);
        localityTxt = (TextView)itemView.findViewById(R.id.locality);
        logo = (ImageView)itemView.findViewById(R.id.logo);
        selectAccLL = (LinearLayout)itemView.findViewById(R.id.select_acc_ll);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(activity, "Click", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(activity, AddCustomerActivity.class);
//        activity.startActivity(intent);
    }

}
