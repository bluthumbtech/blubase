package com.blubase.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blubase.R;
import com.blubase.helper.UenPreferences;

/**
 * Created by Ketan on 9/10/16.
 */

public class HomeFragment extends Fragment {

    private static final String LOG_TAG = "HomeFragment";
    private View rootView;
    RecyclerView recyclerView;
    private GridLayoutManager lLayout;
    private UenPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        return rootView;
    }

}
