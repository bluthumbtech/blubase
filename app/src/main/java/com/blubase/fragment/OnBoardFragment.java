package com.blubase.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.blubase.R;
import com.blubase.base.AppConstants;
import com.blubase.helper.callbacks.FragmentCallback;

/**
 * Created by aman on 3/12/16.
 */
public class OnBoardFragment extends Fragment {
    private static final String LOG_TAG = "OnBoardFragment";

    private View rootView;
    private Button sign_in, join_us;
    private FragmentCallback fragmentCallback;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCallback) {
            fragmentCallback = (FragmentCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FragmentCallbackListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.on_board_fragment,null);

        sign_in = (Button) rootView.findViewById(R.id.sign_in_bt);
        join_us = (Button) rootView.findViewById(R.id.join_bt);


        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.FRAGMENT_PAGE_CODE_KEY, AppConstants.SIGN_IN_PAGE_TARGET);
                fragmentCallback.onInFragmentButtonClick(bundle);
            }
        });

        join_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.FRAGMENT_PAGE_CODE_KEY, AppConstants.JOIN_US_PAGE_TARGET);
                fragmentCallback.onInFragmentButtonClick(bundle);
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
