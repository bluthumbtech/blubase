package com.blubase.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blubase.R;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

/**
 * Created by aman on 8/12/16.
 */

public class CartOrderCardViewHolder extends RecyclerView.ViewHolder {
    private static final String LOG_TAG = "CartOrderCardViewHolder";

    private Context context;
    private LinearLayout cart_order_detail_ll, cart_order_quantity_ll;
    private ImageView cart_order_image, cart_order_quantity_minus_image, cart_order_quantity_plus_image;
    private Button order_bt;
    private TextView cart_order_name, cart_order_price_txt, cart_order_quantity_txt, cart_order_seller_txt, cart_order_date_txt;
    private View cart_order_card_dotted_line_view;

    private int quantity = 1;

    public CartOrderCardViewHolder(Context context, ViewGroup parent, LayoutInflater inflater) {
        super(getViewHolderView(inflater, parent));
        initView(itemView);
        this.context = context;
        cart_order_card_dotted_line_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    private void initView(View itemView) {
        cart_order_detail_ll = (LinearLayout) itemView.findViewById(R.id.cart_order_detail_ll);
        cart_order_quantity_ll = (LinearLayout) itemView.findViewById(R.id.cart_order_quantity_ll);
        cart_order_name = (TextView) itemView.findViewById(R.id.cart_order_name);
        cart_order_price_txt = (TextView) itemView.findViewById(R.id.cart_order_price_txt);
        cart_order_quantity_txt = (TextView) itemView.findViewById(R.id.cart_order_quantity_txt);
        cart_order_seller_txt = (TextView) itemView.findViewById(R.id.cart_order_seller_txt);
        cart_order_date_txt = (TextView) itemView.findViewById(R.id.cart_order_date_txt);
        cart_order_image = (ImageView) itemView.findViewById(R.id.cart_order_image);
        cart_order_quantity_minus_image = (ImageView) itemView.findViewById(R.id.cart_order_quantity_minus_image);
        cart_order_quantity_plus_image = (ImageView) itemView.findViewById(R.id.cart_order_quantity_plus_image);
        cart_order_card_dotted_line_view = itemView.findViewById(R.id.cart_order_card_dotted_line_view);
    }

    private static View getViewHolderView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.cart_order_card_view_holder, parent, false);
    }

    public void setData(final JSONObject order){
        try {
            if (order != null) {
                if (order.getString("pic") != null && order.getString("pic").length() > 0) {
                    Picasso.with(context).load(order.getString("pic")).into(cart_order_image);
                }else {
                    Picasso.with(context).load(R.drawable.profile_pic).into(cart_order_image);
                }

                cart_order_name.setText(order.getString("nm"));
                cart_order_price_txt.setText(context.getResources().getString(R.string.rupee_symbol)+order.getString("sp")+"/-");
               // cart_order_seller_txt.setText("Seller :"+order.getString("slr"));

                Spannable seller_text = new SpannableString("Seller: ");
                seller_text.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.Black)), 0, seller_text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                cart_order_seller_txt.setText(seller_text);
                Spannable seller_name = new SpannableString(order.getString("slr"));
                seller_name.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.app_color)), 0, seller_name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                cart_order_seller_txt.append(seller_name);

                cart_order_date_txt.setText(order.getString("tm"));

                cart_order_quantity_txt.setText(String.valueOf(quantity));

                cart_order_quantity_minus_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (quantity > 1) {
                            quantity --;
                            cart_order_quantity_txt.setText(String.valueOf(quantity));
                        }
                    }
                });

                cart_order_quantity_plus_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (quantity < 10) {
                            quantity ++;
                            cart_order_quantity_txt.setText(String.valueOf(quantity));
                        }
                    }
                });

            }
        }catch (Exception e){}
    }
}
