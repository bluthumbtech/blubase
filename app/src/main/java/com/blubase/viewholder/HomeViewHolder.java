package com.blubase.viewholder;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blubase.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ketan on 3/10/16.
 */

public class HomeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public LinearLayout tile;
    public TextView titleTxt, descTxt, iconTxt;
    public CircleImageView icon;
    Activity activity;

    public HomeViewHolder(View itemView, Activity activity) {
        super(itemView);
        itemView.setOnClickListener(this);
        this.activity = activity;
        tile = (LinearLayout)itemView.findViewById(R.id.tile);
        titleTxt = (TextView)itemView.findViewById(R.id.home_title);
        descTxt = (TextView)itemView.findViewById(R.id.home_desc);
        iconTxt = (TextView)itemView.findViewById(R.id.icon_txt);
        icon = (CircleImageView)itemView.findViewById(R.id.home_icon);
    }

    @Override
    public void onClick(View view) {
//        Intent intent = new Intent(activity, AddCustomerActivity.class);
//        activity.startActivity(intent);
    }

}
