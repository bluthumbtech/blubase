-keep class bluthumb.common.network.** { *; }
-dontwarn bluthumb.common.network.**

# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }

# okhttp specific classes
-dontwarn com.squareup.okhttp.**
-keep class com.squareup.okhttp.** { *; }
-dontwarn okio.*

-keep class com.uen.model.** { *; }

-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

-keep class com.google.common.**
-dontwarn com.google.common.**

-keepattributes EnclosingMethod

-keep class com.uen.db.** { *; }
-keep class com.uen.helper.** { *; }

-keepclasseswithmembernames public class com.orm.**
-keepclasseswithmembernames public class * extends com.orm.SugarRecord

-keep class * extends com.raizlabs.android.dbflow.config.DatabaseHolder { *; }