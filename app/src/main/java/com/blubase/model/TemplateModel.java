package com.blubase.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ketan on 9/11/16.
 */

public class TemplateModel implements Parcelable {

    String id;
    String name;

    public TemplateModel(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }

    // Creator
    public static final Parcelable.Creator CREATOR
            = new Parcelable.Creator() {
        public TemplateModel createFromParcel(Parcel in) {
            return new TemplateModel(in);
        }

        public TemplateModel[] newArray(int size) {
            return new TemplateModel[size];
        }
    };

    public TemplateModel(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

}
