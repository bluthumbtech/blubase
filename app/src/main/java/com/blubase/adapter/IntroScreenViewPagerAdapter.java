package com.blubase.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.blubase.activity.IntroActivity;
import com.blubase.fragment.IntroFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aman on 5/12/16.
 */

public class IntroScreenViewPagerAdapter extends FragmentPagerAdapter {
    private static final String LOG_TAG = "IntroViewPagerAdapter";

    private List<IntroActivity.IntroScreen> introScreens = new ArrayList<>();

    public IntroScreenViewPagerAdapter(FragmentManager manager, List<IntroActivity.IntroScreen> introScreenList) {
        super(manager);
        this.introScreens = introScreenList;
    }

    @Override
    public Fragment getItem(int position) {

        IntroFragment fragment = new IntroFragment();
        Bundle bundle = new Bundle();
        bundle.putString("intro_heading", introScreens.get(position).heading);
        bundle.putString("intro_title", introScreens.get(position).title);
        bundle.putInt("intro_image", introScreens.get(position).introImage);
        bundle.putInt("intro_color", introScreens.get(position).introColor);

        if (position == introScreens.size()-1) {
            bundle.putString("enter_button", "Get Started");
        }else {
            bundle.putString("enter_button", "Next");
        }
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        if (introScreens != null && introScreens.size() > 0) {
            return introScreens.size();
        }else {
            return 0;
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }


}