package com.blubase.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONObject;
import com.blubase.viewholder.AdCardViewHolder;
import com.blubase.viewholder.MainFeedCardViewHolder;
import com.blubase.viewholder.ProfileHeaderViewHolder;
import com.blubase.viewholder.SimpleTextViewHolder;

import static com.blubase.base.AppConstants.AD_CARD;
import static com.blubase.base.AppConstants.POST_CARD;
import static com.blubase.base.AppConstants.PROFILE_HEADER_CARD;
import static com.blubase.base.AppConstants.SIMPLE_TEXT;

/**
 * Created by aman on 6/12/16.
 */

public class MainFeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String LOG_TAG = "MainFeedAdapter";

    private Context context;
    private LayoutInflater layoutInflater;
    private JSONArray feed;


    public MainFeedAdapter(Context context, JSONArray jsonArray){
        this.context = context;
        this.feed = jsonArray;

        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    public void updateFeed(JSONArray jsonArray){
        this.feed = jsonArray;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case POST_CARD:
                viewHolder = new MainFeedCardViewHolder(context, parent, layoutInflater);
                break;
            case PROFILE_HEADER_CARD:
                viewHolder = new ProfileHeaderViewHolder(context, parent, layoutInflater);
                break;
            case AD_CARD:
                viewHolder = new AdCardViewHolder(context, parent, layoutInflater);
                break;
            case SIMPLE_TEXT:
                viewHolder = new SimpleTextViewHolder(context, parent, layoutInflater);
                break;
            default:
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case POST_CARD:
                MainFeedCardViewHolder mainFeedCardViewHolder = (MainFeedCardViewHolder) holder;
                try {
                    mainFeedCardViewHolder.setData(feed.getJSONObject(position));
                }catch (Exception e){}
                break;
            case PROFILE_HEADER_CARD:
                ProfileHeaderViewHolder profileHeaderViewHolder = (ProfileHeaderViewHolder) holder;
                try {
                    profileHeaderViewHolder.setData(feed.getJSONObject(position));
                }catch (Exception e){}
                break;
            case AD_CARD:
                AdCardViewHolder adCardViewHolder = (AdCardViewHolder) holder;
                try {
                    adCardViewHolder.setData(feed.getJSONObject(position));
                }catch (Exception e){}
                break;
            case SIMPLE_TEXT:
                SimpleTextViewHolder simpleTextViewHolder = (SimpleTextViewHolder) holder;
                try {
                    simpleTextViewHolder.setData(feed.getJSONObject(position));
                }catch (Exception e){}
                break;
            default:
        }

    }

    @Override
    public int getItemCount() {
        if (feed != null && feed.length() > 0) {
            return feed.length();
        }else  {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (feed != null && feed.length() > 0 && position < feed.length()) {
            try {
                JSONObject jsonObject = (JSONObject) feed.get(position);
                if ("pc".equals(jsonObject.getString("tt"))) {
                    return POST_CARD;
                } else if ("pf".equals(jsonObject.getString("tt"))){
                    return PROFILE_HEADER_CARD;
                }else if ("ad".equals(jsonObject.getString("tt"))){
                    return AD_CARD;
                }else if ("st".equals(jsonObject.getString("tt"))){
                    return SIMPLE_TEXT;
                }else {
                    return -1;
                }
            }catch (Exception e){
                return -1;
            }
        }else  {
            return -1;
        }
    }
}
