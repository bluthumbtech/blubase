package com.blubase.helper.call_logs;

/**
 * Created by Ketan on 6/10/16.
 */

public interface PhoneStateCallback {
    public void onCellularSignalChange(int rssi);
}
