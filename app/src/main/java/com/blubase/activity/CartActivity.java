package com.blubase.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blubase.R;
import com.blubase.adapter.CartOrderListAdapter;
import com.blubase.helper.AppUtil;

import org.json.JSONArray;
import org.json.JSONObject;

public class CartActivity extends BaseActivity {
    private static final String LOG_TAG = "CartActivity";
    private LinearLayout toolbar, cart_user_info_ll;
    private RecyclerView orderRecyclerView;
    private EditText cart_price_detail_coupon_apply_edt;
    private Button cart_pay_bt;
    private TextView pageTitle, cart_user_name_txt, cart_user_address_txt, cart_user_mobile_txt, cart_user_change_txt, cart_price_detail_price_txt,
            cart_price_detail_delivery_charge_txt, cart_price_detail_total_price_txt, cart_price_detail_coupon_apply_txt,
            cart_price_detail_discount_approved_txt, cart_price_detail_total_payable_amount_txt;
    private View cart_price_detail_dotted_line_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        initViews();

        pageTitle.setText("My Cart");
        cart_price_detail_dotted_line_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        String profileString  = AppUtil.loadJSONFromAsset(this, "order_test.json");
        JSONObject json = null;
        JSONArray orders = null;
        try {
            json = new JSONObject(profileString);
            orders = json.getJSONArray("ors");
        }catch (Exception e){}

        //setViewsData(profile);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        orderRecyclerView.setLayoutManager(layoutManager);
        orderRecyclerView.setNestedScrollingEnabled(false);
        CartOrderListAdapter adapter = new CartOrderListAdapter(this, orders);
        orderRecyclerView.setAdapter(adapter);

        cart_price_detail_price_txt.setText(getResources().getString(R.string.rupee_symbol)+"70000"+"/-");
        cart_price_detail_total_price_txt.setText(getResources().getString(R.string.rupee_symbol)+"70000"+"/-");
        cart_price_detail_total_payable_amount_txt.setText(getResources().getString(R.string.rupee_symbol)+"70000"+"/-");
    }

    private void initViews() {
        toolbar = (LinearLayout) findViewById(R.id.common_toolbar);
        pageTitle = (TextView) findViewById(R.id.toolbar_title);
        cart_price_detail_price_txt = (TextView) findViewById(R.id.cart_price_detail_price_txt);
        cart_price_detail_total_price_txt = (TextView) findViewById(R.id.cart_price_detail_total_price_txt);
        cart_price_detail_total_payable_amount_txt = (TextView) findViewById(R.id.cart_price_detail_total_payable_amount_txt);
        orderRecyclerView = (RecyclerView) findViewById(R.id.cart_order_recycler_view);
        cart_price_detail_dotted_line_view = findViewById(R.id.cart_price_detail_dotted_line_view);
    }
}
