package com.blubase.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blubase.R;
import com.blubase.adapter.MainFeedAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends BaseActivity {
    private static final String LOG_TAG = "ProfileActivity";
    private LinearLayout toolbar;
    private RecyclerView profileRecyclerView;
    private CircleImageView profileImage;
    private LinearLayout profileSearchLl,profileInfo;
    private ImageView profileShare, profileMenu;
    private TextView pageTitle, profileName, profileEmail, profileDetails, profile_info_post_txt, profile_info_hits_txt, profile_info_follow_txt, profile_info_follower_txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_new);

        initViews();

        pageTitle.setText("Profile");

        String profileString  = loadJSONFromAsset(this);
        JSONObject json = null;
        JSONObject profile = null;
        JSONArray feed = null;
        try {
            json = new JSONObject(profileString);
            profile = json.getJSONObject("pf");
            feed = json.getJSONArray("feed");
        }catch (Exception e){}

        //setViewsData(profile);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        profileRecyclerView.setLayoutManager(layoutManager);
        MainFeedAdapter adapter = new MainFeedAdapter(this, feed);
        profileRecyclerView.setAdapter(adapter);

    }

    private void setViewsData(final JSONObject profile) {
        if (profile != null) {

            try {
                if (profile.getString("pic") != null && profile.getString("pic").length() > 0) {
                    Picasso.with(this).load(profile.getString("pic")).into(profileImage);
                }else {
                    Picasso.with(this).load(R.drawable.profile_pic).into(profileImage);
                }

                profileName.setText(profile.getString("nm"));
                profileEmail.setText(profile.getString("uid"));
                profileDetails.setText(profile.getString("ab"));

                profile_info_post_txt.setText(profile.getJSONObject("info").getString("post"));
                profile_info_hits_txt.setText(profile.getJSONObject("info").getString("hits"));
                profile_info_follow_txt.setText(profile.getJSONObject("info").getString("fl"));
                profile_info_follower_txt.setText(profile.getJSONObject("info").getString("flr"));

                profileMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showPopupList(profile, profileMenu);
                    }
                });

            }catch (Exception e){}
        }
    }

    private void initViews() {
        toolbar = (LinearLayout) findViewById(R.id.common_toolbar);
        pageTitle = (TextView) findViewById(R.id.toolbar_title);
        profileRecyclerView = (RecyclerView) findViewById(R.id.profile_rv_list);
       /* profileImage = (CircleImageView) findViewById(R.id.profile_iv);
        profileSearchLl = (LinearLayout) findViewById(R.id.profile_main_search_ll);
        profileInfo = (LinearLayout) findViewById(R.id.profile_info_ll);
        profileShare = (ImageView) findViewById(R.id.profile__share_iv);
        profileMenu = (ImageView) findViewById(R.id.profile_over_flow_iv);
        profileName = (TextView) findViewById(R.id.profile_name_txt);
        profileEmail = (TextView) findViewById(R.id.profile_email_txt);
        profileDetails = (TextView) findViewById(R.id.profile_detail_txt);
        profile_info_post_txt = (TextView) findViewById(R.id.profile_info_post_txt);
        profile_info_hits_txt = (TextView) findViewById(R.id.profile_info_hits_txt);
        profile_info_follow_txt = (TextView) findViewById(R.id.profile_info_follow_txt);
        profile_info_follower_txt = (TextView) findViewById(R.id.profile_info_follower_txt);*/

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }*/

    private void showPopupList(JSONObject jsonObject, View anchorView) {
        try {
            PopupMenu popupMenu = new PopupMenu(this, anchorView);
            JSONArray menu = jsonObject.getJSONArray("menu");
            for (int i=0; i < menu.length(); i++){
                popupMenu.getMenu().add(menu.getJSONObject(i).getString("mt"));
            }
            popupMenu.show();
        }catch (Exception e){}
    }


    public static String loadJSONFromAsset(Context context) {
        String json;
        try {
            InputStream is = context.getAssets().open("test_profile.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (Exception ex) {
            Log.e(LOG_TAG, " Exception loadJSONFromAsset - ", ex);
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
