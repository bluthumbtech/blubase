package com.blubase.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.blubase.model.UserModel;

import bluthumb.common.logs.BLog;

/**
 * Created by Ketan on 1/9/16.
 */
public class UenPreferences {

    private static final String LOG_TAG = "NautankiPreferences";
    private static String TAG = UenPreferences.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "nautanki_pref";
    private static final String KEY_FIRST_TIME = "first_time";
    private static final String KEY_MULTIPLE_DEVICES = "multiple_devices";

    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_NAME = "name";
    private static final String KEY_USER_ID = "userId";
    private static final String KEY_ROLE_ID = "roleId";
    private static final String KEY_ROLE_NAME = "roleName";
    private static final String KEY_BUSINESS_ID = "businessId";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_LOGO = "logo";
    private static final String KEY_LAT = "lat";
    private static final String KEY_LONG = "long";
    private static final String KEY_CITY = "city";
    private static final String KEY_LOCALITY = "locality";
    private static final String KEY_ADDRESS = "address";

    private static final String KEY_VIBRATE = "is_vibrate";
    private static final String KEY_PERMISSION_GRANTED = "is_permission_granted";

    public UenPreferences(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        // commit changes
        editor.commit();
        BLog.d(TAG, "User login session modified!");
    }

    public void setFirstTime(boolean isFirstTime){
        editor.putBoolean(KEY_FIRST_TIME, isFirstTime);
        editor.commit();
    }

    public void setMultipleDevices(boolean isFirstTime){
        editor.putBoolean(KEY_MULTIPLE_DEVICES, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTime(){
        return pref.getBoolean(KEY_FIRST_TIME, true);
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public void clearPreferences(){
        editor.clear();
        editor.commit();
        setFirstTime(false);
    }

    public void setUserData(UserModel userData){
        editor.putString(KEY_USERNAME, userData.getUserName());
        editor.putString(KEY_NAME, userData.getName());
        editor.putString(KEY_PASSWORD, userData.getPassword());
        editor.putString(KEY_USER_ID, userData.getUserId());
        editor.putString(KEY_ROLE_ID, userData.getRoleId());
        editor.putString(KEY_ROLE_NAME, userData.getRoleName());
        editor.putString(KEY_BUSINESS_ID, userData.getBusinessId());
        editor.putString(KEY_CATEGORY, userData.getCategory());
        editor.putString(KEY_LOGO, userData.getLogo());
        editor.putString(KEY_LAT, userData.getLat());
        editor.putString(KEY_LONG, userData.getLng());
        editor.putString(KEY_CITY, userData.getCity());
        editor.putString(KEY_LOCALITY, userData.getLocality());
        editor.putString(KEY_ADDRESS, userData.getAddress());
        editor.commit();
    }

    public void setUserDataWithPassword(UserModel userData){
        editor.putString(KEY_USERNAME, userData.getUserName());
        editor.putString(KEY_NAME, userData.getName());
        editor.putString(KEY_PASSWORD, userData.getPassword());
        editor.putString(KEY_USER_ID, userData.getUserId());
        editor.putString(KEY_ROLE_ID, userData.getRoleId());
        editor.putString(KEY_ROLE_NAME, userData.getRoleName());
        editor.putString(KEY_BUSINESS_ID, userData.getBusinessId());
        editor.putString(KEY_CATEGORY, userData.getCategory());
        editor.putString(KEY_LOGO, userData.getLogo());
        editor.putString(KEY_LAT, userData.getLat());
        editor.putString(KEY_LONG, userData.getLng());
        editor.putString(KEY_CITY, userData.getCity());
        editor.putString(KEY_LOCALITY, userData.getLocality());
        editor.putString(KEY_ADDRESS, userData.getAddress());
        editor.commit();
    }

    public void setUserDataWithoutPassword(UserModel userData){
        editor.putString(KEY_USERNAME, userData.getUserName());
        editor.putString(KEY_NAME, userData.getName());
        editor.putString(KEY_USER_ID, userData.getUserId());
        editor.putString(KEY_ROLE_ID, userData.getRoleId());
        editor.putString(KEY_ROLE_NAME, userData.getRoleName());
        editor.putString(KEY_BUSINESS_ID, userData.getBusinessId());
        editor.putString(KEY_CATEGORY, userData.getCategory());
        editor.putString(KEY_LOGO, userData.getLogo());
        editor.putString(KEY_LAT, userData.getLat());
        editor.putString(KEY_LONG, userData.getLng());
        editor.putString(KEY_CITY, userData.getCity());
        editor.putString(KEY_LOCALITY, userData.getLocality());
        editor.putString(KEY_ADDRESS, userData.getAddress());
        editor.commit();
    }

    public void setUserPassword(String password){
        editor.putString(KEY_PASSWORD, password);
        editor.commit();
    }

    public String getUserPassword(){
        return pref.getString(KEY_PASSWORD,"");
    }

    public UserModel getUserData(){
        UserModel userModel = new UserModel();
        userModel.setUserName(pref.getString(KEY_USERNAME,""));
        userModel.setName(pref.getString(KEY_NAME,""));
        userModel.setPassword(pref.getString(KEY_PASSWORD,""));
        userModel.setRoleId(pref.getString(KEY_ROLE_ID,""));
        userModel.setRoleName(pref.getString(KEY_ROLE_NAME, ""));
        userModel.setUserId(pref.getString(KEY_USER_ID,""));
        userModel.setBusinessId(pref.getString(KEY_BUSINESS_ID,""));
        userModel.setCategory(pref.getString(KEY_CATEGORY,""));
        userModel.setLogo(pref.getString(KEY_LOGO,""));
        userModel.setLat(pref.getString(KEY_LAT,""));
        userModel.setLng(pref.getString(KEY_LONG, ""));
        userModel.setCity(pref.getString(KEY_CITY,""));
        userModel.setLocality(pref.getString(KEY_LOCALITY, ""));
        userModel.setAddress(pref.getString(KEY_ADDRESS,""));
        return userModel;
    }


    public void setPermissionGranted(boolean isPermissionGranted){
        editor.putBoolean(KEY_PERMISSION_GRANTED, isPermissionGranted);
        editor.commit();
    }

    public boolean isPermissionGranted(){
        return pref.getBoolean(KEY_PERMISSION_GRANTED, false);
    }

    public void setIsVibrate(boolean isVibrate){
        editor.putBoolean(KEY_VIBRATE, isVibrate);
        editor.commit();
    }

    public boolean isVibrate(){
        return pref.getBoolean(KEY_VIBRATE, true);
    }

}
