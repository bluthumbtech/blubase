package com.blubase.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ketan on 1/9/16.
 */
public class UserModel implements Serializable {

    @SerializedName("userId")
    String userId;
    @SerializedName("userName")
    String userName;
    @SerializedName("name")
    String name;
    @SerializedName("roleId")
    String roleId;
    @SerializedName("roleName")
    String roleName;
    @SerializedName("password")
    String password;
    @SerializedName("businessId")
    String businessId;
    @SerializedName("category")
    String category;
    @SerializedName("logo")
    String logo;
    @SerializedName("lat")
    String lat;
    @SerializedName("long")
    String lng;
    @SerializedName("city")
    String city;
    @SerializedName("locality")
    String locality;
    @SerializedName("address")
    String address;

    ArrayList<UserModel> userModelArrayList;

    public UserModel(){}

//    public UserModel(String userId, String userName, String name,
//                     String roleId, String roleName, String password){
//        this.userId = userId;
//        this.userName = userName;
//        this.name = name;
//        this.roleId = roleId;
//        this.roleName = roleName;
//        this.password = password;
//    }

    public UserModel(String userId, String userName, String name, String roleId,
                     String roleName, String password, String businessId, String category,
                     String logo, String lat, String lng, String city,
                     String locality, String address) {
        this.userId = userId;
        this.userName = userName;
        this.name = name;
        this.roleId = roleId;
        this.roleName = roleName;
        this.password = password;
        this.businessId = businessId;
        this.category = category;
        this.logo = logo;
        this.lat = lat;
        this.lng = lng;
        this.city = city;
        this.locality = locality;
        this.address = address;
    }

    public ArrayList<UserModel> getUserModelArrayList() {
        return userModelArrayList;
    }

    public void setUserModelArrayList(ArrayList<UserModel> userModelArrayList) {
        this.userModelArrayList = userModelArrayList;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
