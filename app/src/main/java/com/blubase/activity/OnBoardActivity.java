package com.blubase.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.blubase.R;
import com.blubase.base.AppConstants;
import com.blubase.fragment.LoginFragment;
import com.blubase.fragment.OTPFragment;
import com.blubase.fragment.OnBoardFragment;
import com.blubase.helper.UenPreferences;
import com.blubase.helper.callbacks.FragmentCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bluthumb.common.logs.BLog;

public class OnBoardActivity extends BaseActivity implements FragmentCallback{

    FragmentManager fm;
    FragmentTransaction ft;
    Fragment currentFragment;
    public static final String LOG_TAG = "OnBoardActivity";
    Activity activity;
    UenPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        setContentView(R.layout.activity_on_board);
        preferences = new UenPreferences(this);

        checkPerm();
        fm = getSupportFragmentManager();
        fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                BLog.e(LOG_TAG, "BackStackChanged");
                currentFragment = (Fragment) fm.findFragmentById(R.id.onboard_frame);
                if(currentFragment != null){
                    BLog.e(LOG_TAG, "current_fragment - "+currentFragment);
                }
            }
        });

        addFragment(new OnBoardFragment());

    }

    private void addFragment(Fragment fragment){
        ft = fm.beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        ft.add(R.id.onboard_frame, fragment);
        ft.addToBackStack(fragment.toString());
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if(fm.getBackStackEntryCount() > 1){
            super.onBackPressed();
        }else{
            finish();
        }
    }

    public void destroyTopFragment(String referrer) {
        if(fm == null){
            fm = getSupportFragmentManager();
        }
        BLog.e(LOG_TAG, "referrer - "+referrer);
        fm.popBackStackImmediate();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.PERMISSION_REQUEST_ID:
            {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
//                perms.put(Manifest.permission.WRITE_CONTACTS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.GET_ACCOUNTS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
//                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                perms.put(Manifest.permission.VIBRATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.PROCESS_OUTGOING_CALLS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_CALL_LOG, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_CALL_LOG, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_WIFI_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_NETWORK_STATE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                BLog.e(LOG_TAG, perms.toString());

                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
//                        && perms.get(Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
//                        && perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

                        && perms.get(Manifest.permission.VIBRATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.PROCESS_OUTGOING_CALLS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_CALL_LOG) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    BLog.e(LOG_TAG, "perm true");
                    preferences.setPermissionGranted(true);
                } else {
                    // Permission Denied
                    BLog.e(LOG_TAG, "perm false");
                    preferences.setPermissionGranted(false);
//                        Toast.makeText(SignUpActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
//                                .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(OnBoardActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if(android.os.Build.VERSION.SDK_INT >= 23){
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
            return true;
        }else{
            return false;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPerm(){
        if(android.os.Build.VERSION.SDK_INT >= 23){
            List<String> permissionsNeeded = new ArrayList<String>();
            final List<String> permissionsList = new ArrayList<String>();

            if (!addPermission(permissionsList, Manifest.permission.INTERNET))
                permissionsNeeded.add(null);
            if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                permissionsNeeded.add("Storage");
            if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
                permissionsNeeded.add(null);

//            if (!addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
//                permissionsNeeded.add(null);
//            if (!addPermission(permissionsList, Manifest.permission.READ_CONTACTS))
//                permissionsNeeded.add(null);
//            if (!addPermission(permissionsList, Manifest.permission.GET_ACCOUNTS))
//                permissionsNeeded.add(null);
//
//            if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
//                permissionsNeeded.add("Location");
//            if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
//                permissionsNeeded.add(null);
//            if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
//                permissionsNeeded.add(null);

//            if (!addPermission(permissionsList, Manifest.permission.READ_SMS))
//                permissionsNeeded.add("SMS");
//
//            if (!addPermission(permissionsList, Manifest.permission.ACCESS_WIFI_STATE))
//                permissionsNeeded.add("Network State");
//
//            if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
//                permissionsNeeded.add(null);
//
//            if (!addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
//                permissionsNeeded.add(null);
//
//            if (!addPermission(permissionsList, Manifest.permission.READ_CALL_LOG))
//                permissionsNeeded.add(null);

//            if (!addPermission(permissionsList, Manifest.permission.WRITE_CALL_LOG))
//                permissionsNeeded.add(null);

//            if (!addPermission(permissionsList, Manifest.permission.PROCESS_OUTGOING_CALLS))
//                permissionsNeeded.add(null);

//            if (!addPermission(permissionsList, Manifest.permission.VIBRATE))
//                permissionsNeeded.add("Vibrate Sensor");
//
//            if (!addPermission(permissionsList, Manifest.permission.PROCESS_OUTGOING_CALLS))
//                permissionsNeeded.add(null);
//
//            if (!addPermission(permissionsList, Manifest.permission.WRITE_CALL_LOG))
//                permissionsNeeded.add(null);

            BLog.e(LOG_TAG, "permissionsNeeded size - "+permissionsNeeded.size());
            BLog.e(LOG_TAG, "permissionsNeeded - "+permissionsNeeded);
            BLog.e(LOG_TAG, "permissionsList - "+permissionsList);
            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {
                    BLog.e(LOG_TAG, "res perm - "+preferences.isPermissionGranted());
                    if(!preferences.isPermissionGranted()){
                        String message = "You need to grant access to " + permissionsNeeded.get(0);
                        for (int i = 1; i < permissionsNeeded.size(); i++){
                            if(permissionsNeeded.get(i) != null){
                                message = message + ", " + permissionsNeeded.get(i);
                            }
                        }
                        BLog.e(LOG_TAG, "message - "+message);
                        showMessageOKCancel(message,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                                AppConstants.PERMISSION_REQUEST_ID);
                                    }
                                });

                    }
                    return;
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(LOG_TAG, "onActivityResult - "+LOG_TAG);
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onInFragmentButtonClick(Bundle bundle) {
        if (bundle != null) {
            Fragment fragment = null;
            Bundle bundle1 = null;
            String page_code = bundle.getString(AppConstants.FRAGMENT_PAGE_CODE_KEY, "");
            switch (page_code){
                case AppConstants.HOME_PAGE_TARGET:
                    Intent intent = new Intent(OnBoardActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                case AppConstants.SIGN_IN_PAGE_TARGET:
                    fragment = new LoginFragment();
                    bundle1 = new Bundle();
                    bundle1.putString(AppConstants.FRAGMENT_PAGE_CODE_KEY, AppConstants.SIGN_IN_PAGE_TARGET);
                    fragment.setArguments(bundle1);
                    addFragment(fragment);
                    break;
                case AppConstants.JOIN_US_PAGE_TARGET:
                    fragment = new LoginFragment();
                    bundle1 = new Bundle();
                    bundle1.putString(AppConstants.FRAGMENT_PAGE_CODE_KEY, AppConstants.JOIN_US_PAGE_TARGET);
                    fragment.setArguments(bundle1);
                    addFragment(fragment);
                    break;
                case AppConstants.OTP_PAGE_TARGET:
                    fragment = new OTPFragment();
                    bundle1 = new Bundle();
                    bundle1.putString(AppConstants.FRAGMENT_PAGE_CODE_KEY, AppConstants.OTP_PAGE_TARGET);
                    fragment.setArguments(bundle1);
                    addFragment(fragment);
                    break;
                default:
            }
        }
    }
}
