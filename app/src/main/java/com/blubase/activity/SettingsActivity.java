package com.blubase.activity;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.blubase.R;
import com.blubase.fragment.SettingsFragment;

import bluthumb.common.logs.BLog;

public class SettingsActivity extends BaseActivity {

    FragmentManager fm;
    FragmentTransaction ft;
    Fragment currentFragment;
    public static final String LOG_TAG = "SettingsActivity";
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        fm = getSupportFragmentManager();
        fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                BLog.e(LOG_TAG, "BackStackChanged");
                currentFragment = (Fragment) fm.findFragmentById(R.id.guest_frame);
                if(currentFragment != null){
                    BLog.e(LOG_TAG, "current_fragment - "+currentFragment);
                }
            }
        });
        toolbar = (Toolbar)findViewById(R.id.toolbar_settings_new);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle("Settings");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        addFragment(new SettingsFragment());
    }

    private void addFragment(Fragment fragment){
        ft = fm.beginTransaction();
        ft.replace(R.id.settings_frame, fragment);
        ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if(fm.getBackStackEntryCount() > 1){
            super.onBackPressed();
        }else{
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
