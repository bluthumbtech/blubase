package com.blubase.viewholder;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.blubase.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by aman on 5/12/16.
 */

public class MainFeedCardViewHolder extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener {
    private static final String LOG_TAG = "MainFeedCardViewHolder";

    private Context context;

    private LinearLayout main_feed_card_comment_ll, main_feed_card_comment_feed_ll;
    private CircleImageView main_feed_card_profile_iv, main_feed_card_comment_profile_iv;
    private RoundedImageView main_feed_card_detail_iv;
    private ImageView main_feed_card_over_flow_iv, main_feed_card_like_iv, main_feed_card_dislike_iv, main_feed_card_comment_iv, main_feed_card_share_iv;
    private EditText main_feed_card_comment_profile_edt;
    private TextView main_feed_card_name_txt, main_feed_card_profile_follow_txt, main_feed_card_time_txt, main_feed_card_detail_txt;
    private View main_feed_card_dotted_line_view, main_feed_card_dotted_line_view1;


    public MainFeedCardViewHolder(Context context, ViewGroup parent, LayoutInflater inflater) {
        super(getViewHolderView(inflater, parent));
        this.context = context;
        initView(itemView);
        main_feed_card_dotted_line_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        main_feed_card_dotted_line_view1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    private void initView(View itemView) {
        main_feed_card_profile_iv = (CircleImageView) itemView.findViewById(R.id.main_feed_card_profile_iv);
        main_feed_card_comment_profile_iv = (CircleImageView) itemView.findViewById(R.id.main_feed_card_comment_profile_iv);
        main_feed_card_comment_ll = (LinearLayout) itemView.findViewById(R.id.main_feed_card_comment_ll);
        main_feed_card_comment_feed_ll = (LinearLayout) itemView.findViewById(R.id.main_feed_card_comment_feed_ll);
        main_feed_card_detail_iv = (RoundedImageView) itemView.findViewById(R.id.main_feed_card_detail_iv);
        main_feed_card_over_flow_iv = (ImageView) itemView.findViewById(R.id.main_feed_card_over_flow_iv);
        main_feed_card_like_iv = (ImageView) itemView.findViewById(R.id.main_feed_card_like_iv);
        main_feed_card_dislike_iv = (ImageView) itemView.findViewById(R.id.main_feed_card_dislike_iv);
        main_feed_card_comment_iv = (ImageView) itemView.findViewById(R.id.main_feed_card_comment_iv);
        main_feed_card_share_iv = (ImageView) itemView.findViewById(R.id.main_feed_card_share_iv);
        main_feed_card_comment_profile_edt = (EditText) itemView.findViewById(R.id.main_feed_card_comment_profile_edt);
        main_feed_card_name_txt = (TextView) itemView.findViewById(R.id.main_feed_card_name_txt);
        main_feed_card_profile_follow_txt = (TextView) itemView.findViewById(R.id.main_feed_card_profile_follow_txt);
        main_feed_card_time_txt = (TextView) itemView.findViewById(R.id.main_feed_card_time_txt);
        main_feed_card_detail_txt = (TextView) itemView.findViewById(R.id.main_feed_card_detail_txt);
        main_feed_card_dotted_line_view = itemView.findViewById(R.id.main_feed_card_dotted_line_view);
        main_feed_card_dotted_line_view1 = itemView.findViewById(R.id.main_feed_card_dotted_line_view1);
    }

    private static View getViewHolderView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.main_feed_card_view_holder, parent, false);
    }

    public void setData(final JSONObject jsonObject){
        try {
            if (jsonObject != null) {

                if (jsonObject.getString("pic") != null && jsonObject.getString("pic").length() > 0) {
                    Log.i(LOG_TAG, "pic - "+jsonObject.getString("pic"));
                    Picasso.with(context).load(jsonObject.getString("pic")).into(main_feed_card_profile_iv);
                }else {
                    Picasso.with(context).load(R.drawable.profile_pic).into(main_feed_card_profile_iv);
                }

                if (jsonObject.getString("dpic") != null && jsonObject.getString("dpic").length() > 0) {
                    Log.i(LOG_TAG, "dpic - "+jsonObject.getString("dpic"));
                    Picasso.with(context).load(jsonObject.getString("dpic")).fit().into(main_feed_card_detail_iv);
                }else {
                    Picasso.with(context).load(R.drawable.home_bg).fit().into(main_feed_card_detail_iv);
                }

                main_feed_card_name_txt.setText(jsonObject.getString("nm") + " added a post");
                main_feed_card_time_txt.setText(jsonObject.getString("pt"));
                main_feed_card_detail_txt.setText(jsonObject.getString("ab"));

                main_feed_card_over_flow_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showPopupList(jsonObject, main_feed_card_over_flow_iv);
                    }
                });
            }
        }catch (Exception e){}
    }

    private void showPopupList(JSONObject jsonObject, View anchorView) {
        try {
            PopupMenu popupMenu = new PopupMenu(context, anchorView);
            popupMenu.setOnMenuItemClickListener(this);
            JSONArray menu = jsonObject.getJSONArray("menu");
            for (int i=0; i < menu.length(); i++){
                popupMenu.getMenu().add(menu.getJSONObject(i).getString("mt"));
            }
            popupMenu.show();
        }catch (Exception e){}
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }
}
