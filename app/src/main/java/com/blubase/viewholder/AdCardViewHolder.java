package com.blubase.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.makeramen.roundedimageview.RoundedImageView;
import com.blubase.R;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

/**
 * Created by aman on 6/12/16.
 */

public class AdCardViewHolder extends RecyclerView.ViewHolder {
    private static final String LOG_TAG = "AdCardViewHolder";

    private Context context;
    private RoundedImageView ad_card_iv;

    public AdCardViewHolder(Context context, ViewGroup parent, LayoutInflater inflater) {
        super(getViewHolderView(inflater, parent));
        this.context = context;
        initView(itemView);
    }

    private void initView(View itemView) {
        ad_card_iv = (RoundedImageView) itemView.findViewById(R.id.ad_card_iv);
    }

    private static View getViewHolderView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.main_feed_ad_card, parent, false);
    }


    public void setData(final JSONObject jsonObject){
        try {
            if (jsonObject != null) {

                if (jsonObject.getString("url") != null && jsonObject.getString("url").length() > 0) {
                    Log.i(LOG_TAG, "url - "+jsonObject.getString("url"));
                    Picasso.with(context).load(jsonObject.getString("url")).fit().into(ad_card_iv);
                }else {
                    Picasso.with(context).load(R.drawable.home_bg).into(ad_card_iv);
                }
            }
        }catch (Exception e){}
    }
}
