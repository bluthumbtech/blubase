package com.blubase.helper.retrofit;

import com.google.gson.JsonElement;
import com.blubase.model.response.LoginResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by aman on 1/9/16.
 */
public interface UenApiInterface {

    @GET("login/addo")
    Call<LoginResponse> getLogin(@Query("userName") String username, @Query("password") String password);

    @GET("addoContacts/add")
    Call<JsonElement> addContact(@Query("userId") String userId, @Query("password") String password);

}
