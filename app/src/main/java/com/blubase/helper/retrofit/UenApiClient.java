package com.blubase.helper.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by aman on 1/9/16.
 */
public class UenApiClient {
    private static final String LOG_TAG = "UenApiClient";

    private static String BASE_URL = "http://www.uengage.in/ueapi/";
    private static Retrofit mRetrofit = null;
    private static Gson gson;


    public static Retrofit getClient(){
        if(gson == null){
            gson = new GsonBuilder()
                    .setLenient()
                    .create();
        }
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return mRetrofit;
    }

//    public static Retrofit getRetrofitForUrl(String url){
//        Retrofit retrofit = null;
//        try {
//            retrofit = new Retrofit.Builder()
//                    .baseUrl(url)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//        }catch (Exception e){
//            BLog.e(LOG_TAG, "getRetrofitForUrl :",e);
//        }
//        return retrofit;
//    }
}
